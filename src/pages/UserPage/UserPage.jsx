
import styles from './UserPage.module.scss'
import User from '../../components/Users/user';
import { Grid} from '@mui/material';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {ReactComponent as Like} from '../../staticElem/icons/heart.svg'
import {ReactComponent as Comment} from '../../staticElem/icons/comment.svg'
import { openModal } from '../../stores/actions';
import ModalWindow from '../../components/Modal/Modal';
import { useState } from 'react';
import { useEffect } from 'react';
import { getUsers, getPosts } from '../../stores/actions';
import React from 'react';


function UserPage() {

    const dispatch = useDispatch()
        useEffect(() => {
            dispatch(getUsers());
        }, [dispatch]);

        useEffect(() => {
            dispatch(getPosts());
        }, [dispatch]);

    const {id} = useParams();
    const itemId = Number(id);

    const users = useSelector((state) => state.users);
    const postList = useSelector((state) => state.posts);

    const trackedUsers = useSelector((state) => state.subscribe);
    const isUserSubscribed = trackedUsers.some(user => user.id === itemId);
    const buttonText = isUserSubscribed ? 'Unsubscribe' : 'Subscribe';

    const [activePost, setActivePost] = useState(null);

    let userData = null;
    let userName = null;

    for (let i = 0; i < users.length; i++) {
        if (users[i].id === itemId) {
            userData = users[i];
            userName = users[i].user;
            break;
        }
    }

    const showModal = (item) => {
        dispatch(openModal(true));
        setActivePost(item);
    }


    const userPost = () => { return postList.map((item) => {
        if(item.user === userName){
             return(
                <React.Fragment key={`post-${item.id}`}>
                    <Grid className = {styles.post_photo} key = {item.id} onClick={() => showModal(item)} >
                    <img src={item.url} alt="postPhoto" />
                    <div className={styles.block_overlay} >

                    <div className={styles.icons_container}>
                        <div className={styles.icon_container}>
                            <div className={styles.icon}>
                                <Like/>
                            </div>
                            <span className={styles.counter}>{item.likes}</span>
                        </div>
                        <div className={styles.icon_container}>
                            <div className={styles.icon}>
                                <Comment className={styles.icon_comm}/>
                            </div>
                            <span className={styles.counter}>{item.comments.length}</span>
                        </div>

                    </div>
                    </div>
                    </Grid>
                    <ModalWindow info ={activePost} key={`modal-${item.id}`}/>
                </React.Fragment>

            );
        }
        return null;
    })
    };

    return (

        <div className={styles.wrapper}>

            <div className={styles.header}>
                <User text={buttonText} info ={userData}/>
            </div>
            <div className={styles.post_feed}>
                <Grid container gap={2}>
                    {userPost()}
                </Grid>
            </div>


        </div>
     );
}

export default UserPage;

