
import PostsList from '../../components/PostsList/PostsList';
import styles from './MyNewsFeed.module.scss'
import UserList from '../../components/Users/users';
import SubscriptionList from '../../components/Subcriptions/SubscriptionList';
import {useEffect } from 'react';
import { useDispatch} from 'react-redux';
import { getUsers, getPosts } from '../../stores/actions';


function NewsFeed() {

    const dispatch = useDispatch()

    useEffect(() => {
      dispatch(getUsers());
    }, [dispatch]);

    useEffect(() => {
      dispatch(getPosts());
    }, [dispatch]);

    return (
        <div className={styles.wrapper}>
            <PostsList />
            <div className={styles.side} >
                <SubscriptionList/>
                <UserList/>
            </div>
        </div>
      );
}

export default NewsFeed;