import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from './Header';

describe('Header Component', () => {
  it('renders the correct text', () => {
    render(<Header />);
    const headerElement = screen.getByText('Instagram');
    expect(headerElement).toBeInTheDocument();
  });

  it('has the correct styles', () => {
    render(<Header />);
    const headerElement = screen.getByText('Instagram');

    expect(headerElement).toHaveStyle({
      fontSize: '36px',
      fontFamily: 'Arial, Helvetica, sans-serif',
      fontWeight: '700',
      paddingBottom: '30px',
      textAlign: 'center',
      marginTop: '40px',
      color: 'rgb(5, 5, 32)',
      textTransform: 'uppercase',
    });
  });

  it('matches snapshot', () => {
    const { container } = render(<Header />);
    expect(container).toMatchSnapshot();
  });
});