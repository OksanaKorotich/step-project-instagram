

function Header() {

    const styles = {
        fontSize: '36px',
        fontFamily: 'Arial, Helvetica, sans-serif',
        fontWeight: '700',
        paddingBottom: '30px',
        textAlign: 'center',
        marginTop: '40px',
        color: 'rgb(5, 5, 32)',
        textTransform: 'uppercase'

    }

    return (
        <h1 style={styles}>Instagram</h1>
    );
}

export default Header;