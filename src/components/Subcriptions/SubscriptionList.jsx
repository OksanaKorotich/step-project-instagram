
import styles from './Subscription.module.scss'
import User from '../Users/user';
import { useSelector } from 'react-redux';


function SubscriptionList() {

    const trackedUsers = useSelector((state) => state.subscribe)
    localStorage.setItem('Tracked Users', JSON.stringify(trackedUsers))



    return (
        <div className={styles.wrapper}>
        <p className={styles.title}>Your subscriptions</p>
        {trackedUsers.map((item) => <User  key = {item.id} info ={item} text = 'Unsubscribe'/>)}

    </div>
     );
}

export default SubscriptionList;