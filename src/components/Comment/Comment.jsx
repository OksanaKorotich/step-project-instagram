import styles from './Comment.module.scss'
import PropTypes from 'prop-types'

function Comment({info}) {


    return (
        <div className={styles.comment_wrapper}>
            <div className={styles.comment_author}>
                <div className={styles.comment_photo}>
                    <img src={info.photo} alt="user" />
                </div>
                <span className={styles.span_user}>{info.user}</span>
            </div>

            <p className={styles.comment}>{info.text}</p>
        </div>

     );
}

Comment.propTypes = {
    info: PropTypes.shape({
        user: PropTypes.string.isRequired,
        photo: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired,

}


export default Comment;