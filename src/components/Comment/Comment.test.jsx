import React from 'react';
import { render, screen } from '@testing-library/react';
import Comment from './Comment';

const commentInfo = {
  user: 'John Doe',
  photo: 'user.jpg',
  text: 'This is a comment.',
};

describe('Comment Component', () => {
  it('renders the user name', () => {
    render(<Comment info={commentInfo} />);
    const userNameElement = screen.getByText(commentInfo.user);
    expect(userNameElement).toBeInTheDocument();
  });

  it('renders the user photo', () => {
    render(<Comment info={commentInfo} />);
    const userPhotoElement = screen.getByAltText('user');
    expect(userPhotoElement).toBeInTheDocument();
  });

  it('renders the comment text', () => {
    render(<Comment info={commentInfo} />);
    const commentTextElement = screen.getByText(commentInfo.text);
    expect(commentTextElement).toBeInTheDocument();
  });

  it('matches snapshot', () => {
    render(<Comment info={commentInfo} />);
    const container = screen.container;
    expect(container).toMatchSnapshot();
  });
});
