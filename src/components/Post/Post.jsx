
import styles from './Post.module.scss'
import {ReactComponent as Like} from '../../staticElem/icons/heart.svg'
import Comment from '../Comment/Comment';
import Button from '../Button/Button';
import { Card, CardContent} from '@mui/material';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addToFavorites } from '../../stores/actions';
import PropTypes from 'prop-types'


function Post({infoPost}) {

    const arrayId = useSelector((state) => state.favorites)
    const likePost = () =>{
        if(!arrayId.includes(itemId)){
            dispatch(addToFavorites(itemId))
        }
    }

    const {id:itemId} = infoPost;
    const dispatch = useDispatch();

    const [showComments, setShowComments] = useState('show more');

    const toggleComments =() => {
        if(showComments === 'show more'){
            setShowComments('show less')
        }else{
            setShowComments('show more')
        }
    }


    const commentInPost = () => {
        if (infoPost.comments !== undefined && infoPost.comments.length === 1) {
            const comment = infoPost.comments[0]
            return (
                <Comment info={comment} />
            );
        } else if (infoPost.comments !== undefined && infoPost.comments.length > 1 && showComments === 'show more') {
            const lastComment = infoPost.comments[infoPost.comments.length-1]
            return (
                <>
                    <Comment key={lastComment.id} info={lastComment} />
                    <div className={styles.button_wrapper}>
                        <Button text={showComments} onClick ={toggleComments} />
                    </div>
                </>
            );
        } else if(infoPost.comments !== undefined && infoPost.comments.length > 1 && showComments === 'show less'){
                return(
                    <>
                        {infoPost.comments.map(comment => <Comment key={comment.id} info={comment} />)}
                        <div className={styles.button_wrapper}>
                            <Button text={showComments} onClick={toggleComments}/>
                        </div>
                    </>
                );

        }else{
            return (
                <p>There are no comments under this post yet</p>
            );
        }
    }

    return (

        <Card className={styles.card_wrapper}>
            <CardContent className={styles.wrapper}>
                <div className={styles.post_header}>
                    <div className={styles.name}>
                        <div className={styles.user_photo}>
                            <img src={infoPost.photo} alt="user" />
                        </div>
                        <span className={styles.user_name}>{infoPost.user}</span>
                    </div>
                    <div className={styles.icon_like}>
                    <Like data-testid="like-button" className={arrayId.includes(itemId)? styles.active: null}/>
                    </div>
                </div>
            <div className={styles.post_photo} onDoubleClick={likePost}>
                <img src={infoPost.url} alt={infoPost.id} />
            </div>

            <textarea className={styles.textarea} placeholder='Add your comment'></textarea>
            <div className={styles.comment_wrapper}>
                {commentInPost()}
            </div>
            </CardContent>
        </Card>
     );
}

Post.propTypes = {
    infoPost: PropTypes.shape({
        comments: PropTypes.array.isRequired,
        id: PropTypes.number.isRequired,
        url: PropTypes.string.isRequired,
        photo: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired
    }).isRequired
}

export default Post;