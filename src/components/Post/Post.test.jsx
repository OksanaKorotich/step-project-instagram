import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { useDispatch, useSelector } from 'react-redux';
import Post from './Post';


jest.mock('react-redux', () => ({
  useDispatch: jest.fn(),
  useSelector: jest.fn(),
}));

describe('Post Component', () => {
  beforeEach(() => {
    useDispatch.mockClear();
    useSelector.mockClear();
  });

  it('renders post information correctly', () => {
    const infoPost = {
      id: 1,
      comments: [{ id: 1, user: 'User1', photo: 'user.jpg', text: 'Comment 1' }],
      url: 'post.jpg',
      photo: 'user.jpg',
      user: 'User1',
    };
    useDispatch.mockReturnValue(jest.fn());
    useSelector.mockReturnValue([]);

    render(<Post infoPost={infoPost} />);
  });

  it('handles like button click correctly', () => {
    const infoPost = {
      id: 1,
      comments: [{ id: 1, user: 'User1', photo: 'user.jpg', text: 'Comment 1' }],
      url: 'post.jpg',
      photo: 'user.jpg',
      user: 'User1',
    };


    useDispatch.mockReturnValue(jest.fn());
    useSelector.mockReturnValue([]);
    render(<Post infoPost={infoPost} />);
    const likeButton = screen.getByTestId('like-button');
    fireEvent.click(likeButton);
  });

  it('handles show/hide comments correctly', () => {
    const infoPost = {
      id: 1,
      comments: [
        { id: 1, user: 'User1', photo: 'user.jpg', text: 'Comment 1' },
        { id: 2, user: 'User2', photo: 'user.jpg', text: 'Comment 2' },
      ],
      url: 'post.jpg',
      photo: 'user.jpg',
      user: 'User1',
    };

    useDispatch.mockReturnValue(jest.fn());
    useSelector.mockReturnValue([]);
    render(<Post infoPost={infoPost} />);
    const showMoreButton = screen.getByText('show more');
    fireEvent.click(showMoreButton);
    expect(screen.getByText('Comment 2')).toBeInTheDocument();
    fireEvent.click(showMoreButton);

  });
});
