import styles from './Button.module.scss'
import PropTypes from 'prop-types'

function Button({text, onClick}) {

    return (
        <button className={styles.button} onClick={onClick}>{text}</button>
     );
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}


export default Button;