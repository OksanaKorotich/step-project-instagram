import { render, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Button from './Button';

describe('Button Component', () => {
  it('renders without crashing', () => {
    render(<Button text="Test Button" onClick={() => {}} />);
    expect(screen.getByText('Test Button')).toBeInTheDocument();
  });

  it('renders the correct text', () => {
    render(<Button text="Click Me" onClick={() => {}} />);
    expect(screen.getByText('Click Me')).toBeInTheDocument();
  });

  it('calls onClick when the button is clicked', () => {
    const onClickMock = jest.fn();
    render(<Button text="Test Button" onClick={onClickMock} />);

    const button = screen.getByText('Test Button');
    fireEvent.click(button);

    expect(onClickMock).toHaveBeenCalledTimes(1);
  });

  it('matches snapshot', () => {
    render(<Button text="Test Button" onClick={() => {}} />);
    expect(screen.container).toMatchSnapshot();
  });
});