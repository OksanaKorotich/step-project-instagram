
import styles from './users.module.scss'
import User from './user';
import { useSelector } from 'react-redux';


function UserList() {

    const users = useSelector((state) => state.users)
    const trackedUsers = useSelector((state) => state.subscribe)
    const utrackedUsers = users.filter(obj1 => !trackedUsers.some(obj2 => obj2.id === obj1.id));


    return (
        <div className={styles.wrapper}>
            <p className={styles.title}>We recommend</p>
            {utrackedUsers.map((item) =>
            <User  key = {item.id} info ={item} text = 'Subscribe'/>)}

        </div>
     );
}

export default UserList;