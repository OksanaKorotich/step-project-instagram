import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router-dom';
import User from './user';
import { addToSubscribe, removeFromSubscribe } from '../../stores/actions';

const mockStore = configureStore();

describe('User Component', () => {
  it('renders user with subscribe button', () => {
    const initialState = {
      subscribe: [],
    };
    const store = mockStore(initialState);

    const userInfo = {
      id: 1,
      photo: 'user1.jpg',
      user: 'User 1',
    };

    render(
      <Provider store={store}>
        <MemoryRouter>
          <User text="Subscribe" info={userInfo} />
        </MemoryRouter>
      </Provider>
    );

    const userName = screen.getByText(userInfo.user);
    const subscribeButton = screen.getByText('Subscribe');
    expect(userName).toBeInTheDocument();
    expect(subscribeButton).toBeInTheDocument();
  });

  it('handles subscribe/unsubscribe button click', () => {
    const initialState = {
      subscribe: [],
    };
    const store = mockStore(initialState);

    const userInfo = {
      id: 1,
      photo: 'user1.jpg',
      user: 'User 1',
    };

    render(
      <Provider store={store}>
        <MemoryRouter>
          <User text="Subscribe" info={userInfo} />
        </MemoryRouter>
      </Provider>
    );

    // Клікаємо на кнопку "Subscribe"
    const subscribeButton = screen.getByText('Subscribe');
    fireEvent.click(subscribeButton);

    const actions = store.getActions();
    expect(actions).toEqual([addToSubscribe(userInfo)]);

    // Клікаємо на кнопку "Unsubscribe"
    fireEvent.click(subscribeButton);

    const updatedActions = store.getActions();
    expect(updatedActions).toEqual([addToSubscribe(userInfo), removeFromSubscribe(userInfo)]);
  });
});
