
import styles from './users.module.scss'
import Button from '../Button/Button';
import {Link} from "react-router-dom";
import { useDispatch } from 'react-redux';
import { addToSubscribe, removeFromSubscribe} from '../../stores/actions';
import PropTypes from 'prop-types'

function User({text, info}) {

    const dispatch = useDispatch();


    const toggleSubscribe = () => {
        if(text === "Subscribe"){
            dispatch(addToSubscribe(info));
            text = "Unsubscribe"
        }else if(text === "Unsubscribe"){
            dispatch(removeFromSubscribe(info))
            text = "Subscribe"
        }

    }

    const userId = info && info.id;
    const userPhoto = info && info.photo;
    const userName = info && info.user;

    return (
        <div className={styles.user_wrapper}>
            <Link to ={`/${userId}`} className={styles.link} id ={userId}>
                <div className={styles.user_photo}>
                    <img src={userPhoto} alt="user" />
                </div>
                <span className={styles.user_name}>{userName}</span>
            </Link>
            <Button text = {text} onClick={toggleSubscribe}/>
        </div>
     );
}

User.propTypes = {
    info: PropTypes.shape({
        id: PropTypes.number.isRequired,
        photo: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired
    }).isRequired,
    text: PropTypes.string.isRequired
}

export default User;
