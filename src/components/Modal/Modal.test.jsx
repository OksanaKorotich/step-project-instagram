import React from 'react';
import { render, screen} from '@testing-library/react';
import { useDispatch, useSelector } from 'react-redux';
import ModalWindow from './Modal';

jest.mock('react-redux', () => ({
  useDispatch: jest.fn(),
  useSelector: jest.fn(),
}));

describe('ModalWindow Component', () => {
  const mockDispatch = jest.fn();
  const mockUseSelector = jest.fn();

  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    useSelector.mockImplementation(mockUseSelector);
  });

  it('renders null when info or info.url is missing', () => {
    mockUseSelector.mockReturnValue({ modal: true });
    render(<ModalWindow />);

    const modalContent = screen.queryByTestId('modal-content');
    expect(modalContent).not.toBeInTheDocument();
  });

  it('renders comments when they are present in the info', () => {
    mockUseSelector.mockReturnValue({ modal: true });
    const info = {
      comments: [{ id: 1, text: 'Comment 1' }, { id: 2, text: 'Comment 2' }],
      id: 1,
      url: 'image.jpg',
    };
    render(<ModalWindow info={info} />);

    const comment1 = screen.getByText('Comment 1');
    const comment2 = screen.getByText('Comment 2');

    expect(comment1).toBeInTheDocument();
    expect(comment2).toBeInTheDocument();
  });

  it('renders "No comments" message when comments are not present in the info', () => {
    mockUseSelector.mockReturnValue({ modal: true });
    const info = {
      id: 1,
      url: 'image.jpg',
    };
    render(<ModalWindow info={info} />);

    const noCommentsMessage = screen.getByText('There are no comments under this post yet');
    expect(noCommentsMessage).toBeInTheDocument();
  });

  it('matches snapshot', () => {
    mockUseSelector.mockReturnValue({ modal: true });
    const info = {
      comments: [],
      id: 1,
      url: 'image.jpg',
    };
    const { container } = render(<ModalWindow info={info} />);
    expect(container).toMatchSnapshot();
  });
});
