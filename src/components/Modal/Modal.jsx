
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../stores/actions";
import styles from './Modal.module.scss'
import Comment from "../Comment/Comment";
import PropTypes from 'prop-types'
import { Modal } from "@mui/material";



function ModalWindow({info}) {

    const dispatch = useDispatch();
    const modal = useSelector((state) => state.modal )
    const closeModalWindow = () =>{
        dispatch(closeModal(false))
    }

    if (!info || !info.url) {
        return null;
    }


    const commentInPost = () => {
        if (info.comments !== undefined) {
            return (
                info.comments.map(comment =>

                <Comment key={comment.id} info={comment} />)
            );

        }else{
            return (
                <p>There are no comments under this post yet</p>
            );
        }
    }


    return (
        <div  className={modal? styles.active : styles.hidden} onClick={closeModalWindow}>
            <div className={styles.modal__content} onClick={event => event.stopPropagation()}>
                <div className={styles.photo_post}>
                    <img src={info.url} alt="Post" />
                </div>
                <div >
                    {commentInPost()}
                </div>

            </div>
        </div>
     );
}

Modal.propTypes = {
    info: PropTypes.shape({
        comments: PropTypes.array.isRequired,
        id: PropTypes.number.isRequired,
        url: PropTypes.string.isRequired
    }).isRequired,

}

export default ModalWindow;


