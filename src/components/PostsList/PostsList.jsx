

import styles from './PostsList.module.scss'
import Post from '../Post/Post';
import { useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

function PostsList() {

    const postList = useSelector((state) => state.posts);
    const subscribeList = useSelector((state) => state.subscribe)

    const filteredPosts = postList.filter(post => {
        return subscribeList.some(user => user.user === post.user);
    });

    const [hasMore, setHasMore] = useState(true);
    const [startPost, setStartPost] = useState(3);
    const [arrForLoad, setArrForLoad] = useState(filteredPosts.slice(startPost, startPost + 3));
    const startPosts = filteredPosts.slice(0,3)

    const likedPosts = useSelector((state) => state.favorites)
    localStorage.setItem('Liked posts', JSON.stringify(likedPosts))



    const loadMoreData = () => {
        setStartPost(startPost + 3);
        const newPosts = filteredPosts.slice(startPost, startPost + 3);
        setArrForLoad([...arrForLoad, ...newPosts]);
        if (startPost + 3 >= filteredPosts.length) {
            setHasMore(false);
        }
    }

    return (
        <div className={styles.wrapper}>
            {startPosts.map((item) => <Post key={item.id || uuidv4()} infoPost ={item} />)}
                <InfiniteScroll
                    dataLength={arrForLoad.length}
                    next={loadMoreData}
                    hasMore={hasMore}
                    loader={<h4>Loading...</h4>}
                    endMessage={<p className={styles.endMessage}>No more items...</p>}
                >
                    {arrForLoad.map((item) => <Post  key = {uuidv4()} infoPost ={item} />)}
                </InfiniteScroll>

        </div>
     );
}

export default PostsList;

