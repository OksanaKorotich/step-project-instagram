
import {
    GET_USERS,
    GET_POSTS,
    ADD_TO_SUBSCRIBE,
    OPEN_MODAL,
    CLOSE_MODAL,
    REMOVE_FROM_SUBSCRIBE,
    ADD_TO_FAVORITES
} from "./actions";

const itemInSubscribe = localStorage.getItem('Tracked Users');
const itemInliked = localStorage.getItem('Liked posts');

const defaultState = {
    modal: false,
    users: [],
    posts: [],
    subscribe: itemInSubscribe? JSON.parse(itemInSubscribe) : [],
    favorites: itemInliked? JSON.parse(itemInliked) : []
}

function reducer( state = defaultState, action){
    switch(action.type){
        case GET_USERS:
            return {...state, users: action.payload}
        case GET_POSTS:
            return {...state, posts: action.payload}
        case ADD_TO_SUBSCRIBE:
            return {...state, subscribe: [...state.subscribe, action.payload]}
        case REMOVE_FROM_SUBSCRIBE:
            return {...state, subscribe: [...state.subscribe.filter(c => action.payload.id !== c.id)]}
        case ADD_TO_FAVORITES:
            return {...state, favorites: [...state.favorites, action.payload]}
        case OPEN_MODAL:
            return {...state, modal: action.payload}
        case CLOSE_MODAL:
            return {...state, modal: action.payload}



    default:
        return state
    }
}

export default reducer;
