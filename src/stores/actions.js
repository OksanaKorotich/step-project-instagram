
export const GET_USERS = 'GET_USERS'
export const GET_POSTS = "GET_POSTS"
export const ADD_TO_SUBSCRIBE = "ADD_TO_SUBSCRIBE"
export const OPEN_MODAL = 'OPEN_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'
export const REMOVE_FROM_SUBSCRIBE = 'REMOVE_FROM_SUBSCRIBE'
export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES'


// export const getUsers = () => async (dispatch) => {
//     try {
//       // const response = await fetch('http://localhost:5000/users');
//       const response = await fetch('persons.json');

//       if (!response.ok) {
//         throw new Error(`HTTP-помилка: ${response.status}`);
//       }
//       const data = await response.json();
//       const users = data[0].users;
//       return dispatch({
//         type: GET_USERS,
//         payload: users,
//       });
//     } catch (error) {
//       console.error('Помилка при запиті на сервер:', error);
//     }
//   };

  export const getUsers = () => async(dispatch) => {
    const userList =
    await fetch('persons.json').then((res)=>res.json()).then(data =>
      data.users)

    return dispatch ({
        type: GET_USERS,
        payload: userList
    })
}



// export const getPosts = () => async (dispatch) => {
//     try {
//       const response = await fetch('http://localhost:5000/posts');

//       if (!response.ok) {
//         throw new Error(`HTTP-помилка: ${response.status}`);
//       }

//       const data = await response.json();
//       const posts = data[0].posts;
//       return dispatch({
//         type: GET_POSTS,
//         payload: posts,
//       });
//     } catch (error) {
//       console.error('Помилка при запиті на сервер:', error);
//     }
//   };

export const getPosts = () => async(dispatch) => {
  const postList =
  await fetch('posts.json').then((res)=>res.json()).then(data =>
    data.posts)

  return dispatch ({
      type: GET_POSTS,
      payload: postList
  })
}


export const addToSubscribe = (subscribe) =>{
    return{
        type: ADD_TO_SUBSCRIBE,
        payload: subscribe
    }
}

export const removeFromSubscribe = (subscribe) =>{
    return{
        type: REMOVE_FROM_SUBSCRIBE,
        payload: subscribe
    }
}

export const addToFavorites= (favorites) =>{
    return{
        type: ADD_TO_FAVORITES,
        payload: favorites
    }
}


export const openModal = (modal) =>{
    return{
        type: OPEN_MODAL,
        payload: true
    }
}

export const closeModal = (modal) =>{
    return{
        type: CLOSE_MODAL,
        payload: false
    }
}
