

import { Route, Routes, Navigate} from "react-router-dom";
import NewsFeed from "./pages/MyNewsFeed/MyNewsFeed";
import Header from "./components/Header/Header";
import UserPage from "./pages/UserPage/UserPage";




function App() {
  return (
    <div className="App">
      <Header/>
        <Routes>
          <Route path="/" element={<NewsFeed/>}/>
          <Route path="/:id" element = {<UserPage/>}/>
          <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </div>
  );
}

export default App;
