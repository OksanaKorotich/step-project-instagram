**Technologies used in this project:**

- SCSS
- MongoDb
- React
- Redux
- Infinity scroll


**Project participants:**

Oksana Korotich

**Tasks:**

- General design
- Local storage logic
- Classes and methods
- Functions for working with API
- Creation of the main page
- Creation of the logic of transition to each user's page
- Functions for buttons
- Development of a modal window on its logic
- Creation of collections on MongoDB
- Creating a server
- Deployment to Vercel



 
